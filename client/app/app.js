// TODO: Convert app to an AngularJS app
// TODO: 1. Create an angular module and name it EMS

// TODO: 1.1 Create an IIFE function
// Always use an IIFE, i.e., (function() {})();
(function() {
    // TODO: 1.2 Create the new module
    // Creates a new module
    // When setting (creating) an angular module, you need to specify the second argument ([ ])
    // Without this argument, we are telling Angular that what we want to do is to get an already existing module
    angular
        .module("EMS", [])
})();
